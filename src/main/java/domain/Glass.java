package domain;

import java.util.Arrays;

public class Glass {
	public final static byte NUM_ROWS = 20;
	public final static byte NUM_COLS = 10;
	private final static String FILLED = "*";
	
	private final byte[][] data;
	
	public Glass(byte[][] data) {
		if(data.length != NUM_ROWS) throw new IllegalArgumentException("wrongs number of rows");
		
		this.data = new byte[NUM_ROWS][NUM_COLS];
		for(int row = 0; row < NUM_ROWS; row++) {
			if(data[row].length != NUM_COLS) throw new IllegalArgumentException("wrongs number of cols");
			for(int col = 0; col < NUM_COLS; col++) {
				this.data[row][col] = data[row][col];
			}
		}
	}
	
	public static Glass createFromString(String text) {
		if(text.length() != NUM_COLS * NUM_ROWS) throw new IllegalArgumentException("text has wrong number of chars");
		byte[][] data = new byte[NUM_ROWS][NUM_COLS];
		int textIndex = 0;
		for(int row = NUM_ROWS - 1; row >= 0; row--) {
			for(int col = 0; col < NUM_COLS; col++) {
				String symbol = text.substring(textIndex, textIndex + 1);
				textIndex++;
				data[row][col] = (byte) (FILLED.equals(symbol) ? 1 : 0);
			}
		}
		
		return new Glass(data);
	}

	public boolean isIntersect(Glass glass) {
		for(int row = 0; row < NUM_ROWS; row++) {
			for(int col = 0; col < NUM_COLS; col++) {
				if(data[row][col] == 1 && glass.data[row][col] == 1) {
					return true;
				}
			}
		}
		
		return false;
	}
	
	public Glass union(Glass glass) {
		byte[][] data = new byte[NUM_ROWS][NUM_COLS];
		
		for(int row = 0; row < NUM_ROWS; row++) {
			for(int col = 0; col < NUM_COLS; col++) {
				byte value = (byte) ((this.data[row][col] + glass.data[row][col])  == 0 ? 0 : 1);
				data[row][col] = value;
			}
		}
		
		return new Glass(data);
	}
	
	public byte[][] getData() {
		return data;
	};
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + Arrays.deepHashCode(data);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Glass other = (Glass) obj;
		if (!Arrays.deepEquals(data, other.data))
			return false;
		return true;
	}
}
