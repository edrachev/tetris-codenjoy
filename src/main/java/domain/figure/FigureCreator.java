package domain.figure;

import domain.figure.Figure.FigureData;

public class FigureCreator {
	public static Figure create(String figure, int x, int y) {
		return Figure.createFigure(FigureData.valueOf(figure), x, y);
	}
}
