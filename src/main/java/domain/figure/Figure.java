package domain.figure;

import java.util.Arrays;

import domain.Angle;
import domain.Command;
import domain.Glass;

public class Figure {
	private static int NUM_COLS = 5;
	private static int NUM_ROWS = 5;
	private static int HALF_COLS = 2;
	private static int HALF_ROWS = 2;
	
	private final byte[][] data;
	
	private final int x;
	private final int y;
	
	public static Figure createFigure(FigureData figureData, int x, int y) {
		return new Figure(figureData.getData(), x, y);
	}
	
	Figure(byte[][] data, int x, int y) {
		this.x = x;
		this.y = y;
		
		this.data = new byte[NUM_ROWS][NUM_COLS];
		if(data.length != NUM_ROWS) throw new IllegalArgumentException("wrong number of rows");
		for(int row = 0; row < NUM_ROWS; row++) {
			if(data[row].length != NUM_COLS) throw new IllegalArgumentException("wrong number of cols");
			for(int col = 0; col < NUM_COLS; col++) {
				this.data[row][col] = data[row][col];
			}
		}
	}
	
	private Figure rotate90() {
		byte[][] data = new byte[NUM_ROWS][NUM_COLS];
		for(int row = 0; row < NUM_ROWS; row++) {
			for(int col = 0; col < NUM_COLS; col++) {
				data[col][NUM_COLS - 1 - row] = this.data[row][col];
			}
		}
		
		return new Figure(data, x, y);
	}
	
	public Figure rotate(Angle angle) {
		Figure figure = this;
		switch (angle) {
		case ANGLE_270:
			figure = figure.rotate90();
		case ANGLE_180:
			figure = figure.rotate90();
		case ANGLE_90:
			figure = figure.rotate90();
		case ANGLE_0:
			return figure;
		default:
			throw new IllegalArgumentException("Unsupported angle exeption");
		}
	}
	
	public Figure shiftX(int x) {
		return new Figure(data, this.x + x, y);
	}
	
	public Figure shiftY(int y) {
		return new Figure(data, x, this.y + y);
	} 
	
	public Figure applyCommand(Command command) {
		Figure figure = shiftX(command.getShiftX());
		figure = figure.rotate(command.getAngle());
		return figure;
	}
	
	public Glass dropToGlass(Glass glass) {
		Figure figure = this;
		Figure currentFigure = this;
		while(true) {
			if(!currentFigure.isFigureInGlass() || glass.isIntersect(currentFigure.toGlass())) {
				break;
			} else {
				figure = currentFigure;
				currentFigure = currentFigure.shiftY(1);
			}
		}
		return glass.union(figure.toGlass());
	}
	
	public boolean isFigureInGlass() {
		for(int row = 0; row < NUM_ROWS; row++) {
			for(int col = 0; col < NUM_COLS; col++) {
				if(data[row][col] == 1) {
					int actualY = y + row - HALF_ROWS;
					if(actualY >= Glass.NUM_ROWS) { 
						return false;
					}
					
					int actualX = x + col - HALF_COLS;
					if(actualX < 0 || actualX >= Glass.NUM_COLS) {
						return false;
					}
				}
			}
		}
		
		return true;
	}
	
	public Glass toGlass() {
		if(!isFigureInGlass()) throw new IllegalStateException("figure is not in a glass");
			
		byte[][] data = new byte[Glass.NUM_ROWS][Glass.NUM_COLS];
		
		for(int row = 0; row < NUM_ROWS; row++) {
			int calcualtedY = row + y - HALF_ROWS;
			if(calcualtedY < 0) continue;
			
			for(int col = 0; col < NUM_COLS; col++) {
				if(this.data[row][col] == 1) {
					data[calcualtedY][col + x - HALF_COLS] = 1;
				}
			}
		}
		
		return new Glass(data);
	}
	
	public static enum FigureData {
		O(
				new byte[][]{
					new byte[] {0, 0, 0, 0, 0},
					new byte[] {0, 0, 0, 0, 0},
					new byte[] {0, 0, 1, 1, 0},
					new byte[] {0, 0, 1, 1, 0},
					new byte[] {0, 0, 0, 0, 0}
				}
		),
		I(
				new byte[][]{
					new byte[] {0, 0, 0, 0, 0},
					new byte[] {0, 0, 1, 0, 0},
					new byte[] {0, 0, 1, 0, 0},
					new byte[] {0, 0, 1, 0, 0},
					new byte[] {0, 0, 1, 0, 0}
				}
		),
		L(
				new byte[][]{
					new byte[] {0, 0, 0, 0, 0},
					new byte[] {0, 0, 1, 0, 0},
					new byte[] {0, 0, 1, 0, 0},
					new byte[] {0, 0, 1, 1, 0},
					new byte[] {0, 0, 0, 0, 0}
				}
		),
		J(
				new byte[][]{
					new byte[] {0, 0, 0, 0, 0},
					new byte[] {0, 0, 1, 0, 0},
					new byte[] {0, 0, 1, 0, 0},
					new byte[] {0, 1, 1, 0, 0},
					new byte[] {0, 0, 0, 0, 0}
				}
		),
		S(
				new byte[][]{
					new byte[] {0, 0, 0, 0, 0},
					new byte[] {0, 0, 1, 1, 0},
					new byte[] {0, 1, 1, 0, 0},
					new byte[] {0, 0, 0, 0, 0},
					new byte[] {0, 0, 0, 0, 0}
				}
		),
		Z(
				new byte[][]{
					new byte[] {0, 0, 0, 0, 0},
					new byte[] {0, 1, 1, 0, 0},
					new byte[] {0, 0, 1, 1, 0},
					new byte[] {0, 0, 0, 0, 0},
					new byte[] {0, 0, 0, 0, 0}
				}
		),
		T(
				new byte[][]{
					new byte[] {0, 0, 0, 0, 0},
					new byte[] {0, 0, 1, 0, 0},
					new byte[] {0, 1, 1, 1, 0},
					new byte[] {0, 0, 0, 0, 0},
					new byte[] {0, 0, 0, 0, 0}
				}
		);
		
		private final byte[][] data;
		private FigureData(byte[][] data) {
			this.data = data;
		}
		public byte[][] getData() {
			return data;
		}
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + Arrays.deepHashCode(data);
		result = prime * result + x;
		result = prime * result + y;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Figure other = (Figure) obj;
		if (!Arrays.deepEquals(data, other.data))
			return false;
		if (x != other.x)
			return false;
		if (y != other.y)
			return false;
		return true;
	}
}
