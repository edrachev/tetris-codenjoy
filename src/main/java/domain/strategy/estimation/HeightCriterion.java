package domain.strategy.estimation;

import domain.Glass;

public class HeightCriterion implements EstimationCriterion {

	@Override
	public int estimate(Glass glass) {
		byte[][] data = glass.getData();
		int value = 0;
		for(int row = 0; row < Glass.NUM_ROWS; row++) {
			for(int col = 0; col < Glass.NUM_COLS; col++) {
				if(data[row][col] == 1) {
					value++;
					break;
				}
			}
		}
		
		return value;
	}

}
