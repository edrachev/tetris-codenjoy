package domain.strategy.estimation;

import domain.Glass;

public class FilledRowCriterion implements EstimationCriterion {

	@Override
	public int estimate(Glass glass) {
		int value = 0;
		byte[][] data = glass.getData();
		outer:
		for(int row = 0; row < Glass.NUM_ROWS; row++) {
			for(int col = 0; col < Glass.NUM_COLS; col++) {
				if(data[row][col] == 0) {
					continue outer;
				}
			}
			value++;
		}
		
		return value;
	}
}
