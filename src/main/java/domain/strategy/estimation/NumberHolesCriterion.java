package domain.strategy.estimation;

import domain.Glass;

public class NumberHolesCriterion implements EstimationCriterion {

	@Override
	public int estimate(Glass glass) {
		byte[][] data = glass.getData();
		int value = 0;
		for(int row = 0; row < Glass.NUM_ROWS; row++) {
			boolean gap = false;
			boolean filledCell = false;
			for(int col = 0; col < Glass.NUM_COLS; col++) {
				boolean emptyCell = data[row][col] == 0;
				if(!emptyCell) {
					filledCell = true;
				}
				if(gap) {
					if(!emptyCell) {
						gap = false;
					}
				} else {
					if(emptyCell) {
						gap = true;
						value++;
					}
				}
			}
			
			if(!filledCell) {
				value--;
			}
		}
		
		return value;
	}

}
