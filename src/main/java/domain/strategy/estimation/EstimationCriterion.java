package domain.strategy.estimation;

import domain.Glass;

public interface EstimationCriterion {
	int estimate(Glass glass);
}
