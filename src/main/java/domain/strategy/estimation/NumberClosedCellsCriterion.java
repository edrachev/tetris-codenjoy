package domain.strategy.estimation;

import domain.Glass;

public class NumberClosedCellsCriterion implements EstimationCriterion {

	@Override
	public int estimate(Glass glass) {
		int value = 0;
		byte[][] data = glass.getData();
		boolean[] closedColumns = new boolean[Glass.NUM_COLS];
		for(int row = 0; row < Glass.NUM_ROWS; row++) {
			for(int col = 0; col < Glass.NUM_COLS; col++) {
				boolean filledCell = data[row][col] == 1;
				
				if(closedColumns[col]) {
					if(!filledCell) {
						value++;
					}
				} else {
					if(filledCell) {
						closedColumns[col] = true;
					}
				}
			}
		}
		
		return value;
	}

}
