package domain.strategy.estimation;

public class WeightedEstimationCriteria {
	private final EstimationCriterion criteria;
	private final int weight;
	
	public WeightedEstimationCriteria(EstimationCriterion criteria, int weight) {
		this.criteria = criteria;
		this.weight = weight;
	}
	
	public int getWeight() {
		return weight;
	}
	
	public EstimationCriterion getCriteria() {
		return criteria;
	}
}
