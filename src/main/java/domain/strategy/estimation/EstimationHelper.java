package domain.strategy.estimation;

import java.util.ArrayList;
import java.util.List;

import domain.Glass;

public class EstimationHelper {
	private final static List<WeightedEstimationCriteria> criteria = new ArrayList<WeightedEstimationCriteria>();
	
	static {
		criteria.add(new WeightedEstimationCriteria(new FilledRowCriterion(), 1000));
		criteria.add(new WeightedEstimationCriteria(new HeightCriterion(), -100));
		criteria.add(new WeightedEstimationCriteria(new NumberHolesCriterion(), -10));
		criteria.add(new WeightedEstimationCriteria(new NumberClosedCellsCriterion(), -300));
	} 
	
	public static int estimate(Glass glass) {
		int value = 0;
		for(WeightedEstimationCriteria criterion: criteria) {
			value += criterion.getWeight() * criterion.getCriteria().estimate(glass);
		}
		return value;
	}
}
