package domain.strategy;

import domain.Command;
import domain.Glass;
import domain.figure.Figure;

public interface Strategy {
	Command apply(Glass glass, Figure figure, Figure nextFigure);
}
