package domain.strategy;

import java.util.List;
import java.util.Random;

import domain.Command;
import domain.Glass;
import domain.figure.Figure;

public class RandomStrategy implements Strategy {
	private static final Random random = new Random();
	
	@Override
	public Command apply(Glass glass, Figure figure, Figure nextFigure) {
		List<Command> commands = CommandGenerator.generate();
		return commands.get(random.nextInt(commands.size()));
	}

}
