package domain.strategy;

import java.util.ArrayList;
import java.util.List;

import domain.Angle;
import domain.Command;

public class CommandGenerator {
	private final static int EDGE = 5;
	private static List<Command> commandsList;
	
	public static List<Command> generate() {
		if(commandsList == null) {
			commandsList = new ArrayList<Command>();
			for(int shiftX = -EDGE;  shiftX <= EDGE; shiftX++) {
				for(Angle angle: Angle.values()) {
					commandsList.add(new Command(shiftX, angle, true));
				}
			}
		}
		
		return commandsList;
	}
}
