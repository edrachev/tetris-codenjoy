package domain.strategy;

import java.util.ArrayList;
import java.util.List;

import domain.Command;
import domain.Glass;
import domain.figure.Figure;
import domain.strategy.estimation.EstimationHelper;

public class BustStrategy implements Strategy {

	@Override
	public Command apply(Glass glass, Figure figure, Figure nextFigure) {
		int initialGlassEstimation = EstimationHelper.estimate(glass);
		List<Command> commands = new ArrayList<Command>(CommandGenerator.generate());
		int bestValue = Integer.MIN_VALUE;
		Command bestCommand = Command.JUST_DROP;
		for(Command command: commands) {
			Figure updatedFigure = figure.applyCommand(command);
			if(!updatedFigure.isFigureInGlass()) {
				continue;
			}
			if(!glass.isIntersect(updatedFigure.toGlass())) {
				Glass updatedGlass = updatedFigure.dropToGlass(glass);
				int updatedGlassEstimation = EstimationHelper.estimate(updatedGlass);
				int finalGlassEstimation = updatedGlassEstimation - initialGlassEstimation;
				
				List<Command> nextCommands = new ArrayList<Command>(CommandGenerator.generate());
				for(Command nextCommand: nextCommands) {
					Figure updatedNextFigure = nextFigure.applyCommand(nextCommand);
					if(!updatedNextFigure.isFigureInGlass()) {
						continue;
					}
					if(!updatedGlass.isIntersect(updatedNextFigure.toGlass())) {
						Glass updatedNextGlass = updatedNextFigure.dropToGlass(updatedGlass);
						int updatedNextGlassEstimation = EstimationHelper.estimate(updatedNextGlass);
						int finalNextGlassEstimation = updatedNextGlassEstimation - finalGlassEstimation;
						int finalEstimation = finalGlassEstimation + finalNextGlassEstimation;
						
						if(finalEstimation > bestValue) {
							bestValue = finalEstimation;
							bestCommand = command;
						}
					}
				}

			}
		}
		
		return bestCommand;
	}

}
