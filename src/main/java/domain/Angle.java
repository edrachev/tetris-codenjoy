package domain;

public enum Angle {
	ANGLE_0(0), ANGLE_90(1), ANGLE_180(2), ANGLE_270(3);
	
	private final int shift;
	private Angle(int shift) {
		this.shift = shift;
	}
	public int getShift() {
		return shift;
	}
}
