package domain;

import java.util.ArrayList;
import java.util.List;

import com.google.common.base.Joiner;

public class Command {
	private final int shiftX;
	private final Angle angle;
	private final boolean drop;

	public static final Command JUST_DROP = new Command(0, Angle.ANGLE_0, true);
	
	public Command(int shiftX, Angle angle, boolean drop) {
		super();
		this.shiftX = shiftX;
		this.angle = angle;
		this.drop = drop;
	}
	
	public String toCommandString() {
		List<String> commands = new ArrayList<String>(3);
		commands.add("rotate=" + angle.getShift());
		if(shiftX > 0) {
			commands.add("right=" + shiftX);
		} else if(shiftX < 0) {
			commands.add("left=" + (-shiftX));
		}
		
		if(drop) {
			commands.add("drop");
		}
		
		return Joiner.on(',').join(commands);
	}
	
	public int getShiftX() {
		return shiftX;
	}
	
	public Angle getAngle() {
		return angle;
	}
	
	public boolean isDrop() {
		return drop;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((angle == null) ? 0 : angle.hashCode());
		result = prime * result + (drop ? 1231 : 1237);
		result = prime * result + shiftX;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Command other = (Command) obj;
		if (angle != other.angle)
			return false;
		if (drop != other.drop)
			return false;
		if (shiftX != other.shiftX)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Command [shiftX=" + shiftX + ", angle=" + angle + ", drop=" + drop + "]";
	}
}
