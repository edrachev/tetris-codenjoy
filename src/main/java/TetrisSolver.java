import domain.Command;
import domain.Glass;
import domain.figure.Figure;
import domain.figure.FigureCreator;
import domain.strategy.BustStrategy;
import domain.strategy.Strategy;

public class TetrisSolver {
    final static int DO_NOT_ROTATE = 0;
    final static int ROTATE_90_CLOCKWISE = 1;
    final static int ROTATE_180_CLOCKWISE = 2;
    final static int ROTATE_90_COUNTERCLOCKWISE = 3;

    public String answer(String figureText, int x, int y, String glassText, String next) {
    	y = transformY(y);
    	Glass glass = Glass.createFromString(glassText);
    	Figure figure = FigureCreator.create(figureText, x, y);
    	Figure nextFigure = FigureCreator.create(next.substring(0,  1), 4, 0);
    	Strategy strategy = createStrategy();
    	Command command = strategy.apply(glass, figure, nextFigure);
        return command.toCommandString();
    }
    
    private int transformY(int y) {
    	return 20 - y; 
    }
    
    private Strategy createStrategy() {
    	return new BustStrategy();
    	//return new RandomStrategy();
    }
}
