package domain.figure;

import org.junit.Assert;
import org.junit.Test;

import domain.Angle;
import domain.Command;
import domain.Glass;
import domain.figure.Figure.FigureData;

public class FigureTest {
	@Test
	public void rotationTestI() {
		Figure figureI = Figure.createFigure(FigureData.I, 0, 0);
		
		byte[][] data0 = new byte[][]{
			new byte[] {0, 0, 0, 0, 0},
			new byte[] {0, 0, 1, 0, 0},
			new byte[] {0, 0, 1, 0, 0},
			new byte[] {0, 0, 1, 0, 0},
			new byte[] {0, 0, 1, 0, 0}
		};
		
		byte[][] data90 = new byte[][]{
			new byte[] {0, 0, 0, 0, 0},
			new byte[] {0, 0, 0, 0, 0},
			new byte[] {1, 1, 1, 1, 0},
			new byte[] {0, 0, 0, 0, 0},
			new byte[] {0, 0, 0, 0, 0}
		};
		
		byte[][] data180 = new byte[][]{
			new byte[] {0, 0, 1, 0, 0},
			new byte[] {0, 0, 1, 0, 0},
			new byte[] {0, 0, 1, 0, 0},
			new byte[] {0, 0, 1, 0, 0},
			new byte[] {0, 0, 0, 0, 0}
		};
		
		byte[][] data270 = new byte[][]{
			new byte[] {0, 0, 0, 0, 0},
			new byte[] {0, 0, 0, 0, 0},
			new byte[] {0, 1, 1, 1, 1},
			new byte[] {0, 0, 0, 0, 0},
			new byte[] {0, 0, 0, 0, 0}
		};
		
		Figure figureI_0 = new Figure(data0, 0, 0);
		Figure figureI_90 = new Figure(data90, 0, 0);
		Figure figureI_180 = new Figure(data180, 0, 0);
		Figure figureI_270 = new Figure(data270, 0, 0);
		
		Assert.assertEquals("rotation 0 degrees", figureI.rotate(Angle.ANGLE_0), figureI_0);
		Assert.assertEquals("rotation 90 degrees", figureI.rotate(Angle.ANGLE_90), figureI_90);
		Assert.assertEquals("rotation 180 degrees", figureI.rotate(Angle.ANGLE_180), figureI_180);
		Assert.assertEquals("rotation 270 degrees", figureI.rotate(Angle.ANGLE_270), figureI_270);
	}
	
	@Test
	public void rotationTestJ() {
		Figure figureJ = Figure.createFigure(FigureData.J, 0, 0);
		
		byte[][] data0 = new byte[][]{
			new byte[] {0, 0, 0, 0, 0},
			new byte[] {0, 0, 1, 0, 0},
			new byte[] {0, 0, 1, 0, 0},
			new byte[] {0, 1, 1, 0, 0},
			new byte[] {0, 0, 0, 0, 0}
		};
		
		byte[][] data90 = new byte[][]{
			new byte[] {0, 0, 0, 0, 0},
			new byte[] {0, 1, 0, 0, 0},
			new byte[] {0, 1, 1, 1, 0},
			new byte[] {0, 0, 0, 0, 0},
			new byte[] {0, 0, 0, 0, 0}
		};
		
		byte[][] data180 = new byte[][]{
			new byte[] {0, 0, 0, 0, 0},
			new byte[] {0, 0, 1, 1, 0},
			new byte[] {0, 0, 1, 0, 0},
			new byte[] {0, 0, 1, 0, 0},
			new byte[] {0, 0, 0, 0, 0}
		};
		
		byte[][] data270 = new byte[][]{
			new byte[] {0, 0, 0, 0, 0},
			new byte[] {0, 0, 0, 0, 0},
			new byte[] {0, 1, 1, 1, 0},
			new byte[] {0, 0, 0, 1, 0},
			new byte[] {0, 0, 0, 0, 0}
		};
		
		Figure figureJ_0 = new Figure(data0, 0, 0);
		Figure figureJ_90 = new Figure(data90, 0, 0);
		Figure figureJ_180 = new Figure(data180, 0, 0);
		Figure figureJ_270 = new Figure(data270, 0, 0);
		
		Assert.assertEquals("rotation 0 degrees", figureJ.rotate(Angle.ANGLE_0), figureJ_0);
		Assert.assertEquals("rotation 90 degrees", figureJ.rotate(Angle.ANGLE_90), figureJ_90);
		Assert.assertEquals("rotation 180 degrees", figureJ.rotate(Angle.ANGLE_180), figureJ_180);
		Assert.assertEquals("rotation 270 degrees", figureJ.rotate(Angle.ANGLE_270), figureJ_270);
	}
	
	@Test
	public void testIsFigureInGlass() {
		Figure figure1 = Figure.createFigure(FigureData.I, 0, 19);
		Figure figure2 = Figure.createFigure(FigureData.I, 0, 17);
		Figure figure3 = Figure.createFigure(FigureData.I, 0, 15);
		Figure figure4 = Figure.createFigure(FigureData.O, 0, 15);
		Figure figure5 = Figure.createFigure(FigureData.O, 19, 15);
		Assert.assertFalse("is not in glass", figure1.isFigureInGlass());
		Assert.assertTrue("is in glass", figure2.isFigureInGlass());
		Assert.assertTrue("is in glass", figure3.isFigureInGlass());
		Assert.assertTrue("is in glass", figure4.isFigureInGlass());
		Assert.assertFalse("is not in glass", figure5.isFigureInGlass());
	}
	
	@Test
	public void testFigureToGlass() {
		Figure figure = Figure.createFigure(FigureData.I, 5, 14);
		
		byte[][] data = new byte[][] {
			new byte[] {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
			new byte[] {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
			new byte[] {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
			new byte[] {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
			new byte[] {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
			new byte[] {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
			new byte[] {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
			new byte[] {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
			new byte[] {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
			new byte[] {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
			new byte[] {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
			new byte[] {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
			new byte[] {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
			new byte[] {0, 0, 0, 0, 0, 1, 0, 0, 0, 0},
			new byte[] {0, 0, 0, 0, 0, 1, 0, 0, 0, 0},
			new byte[] {0, 0, 0, 0, 0, 1, 0, 0, 0, 0},
			new byte[] {0, 0, 0, 0, 0, 1, 0, 0, 0, 0},
			new byte[] {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
			new byte[] {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
			new byte[] {0, 0, 0, 0, 0, 0, 0, 0, 0, 0}
		};
		Glass glass = new Glass(data);
		
		Assert.assertEquals("figure to glass", figure.toGlass(), glass);
	}
	
	@Test(expected = IllegalStateException.class)
	public void testFigureToGlassFail() {
		Figure figure = Figure.createFigure(FigureData.I, 19, 0);
		
		byte[][] data = new byte[][] {
			new byte[] {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
			new byte[] {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
			new byte[] {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
			new byte[] {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
			new byte[] {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
			new byte[] {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
			new byte[] {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
			new byte[] {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
			new byte[] {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
			new byte[] {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
			new byte[] {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
			new byte[] {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
			new byte[] {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
			new byte[] {0, 0, 0, 0, 0, 1, 0, 0, 0, 0},
			new byte[] {0, 0, 0, 0, 0, 1, 0, 0, 0, 0},
			new byte[] {0, 0, 0, 0, 0, 1, 0, 0, 0, 0},
			new byte[] {0, 0, 0, 0, 0, 1, 0, 0, 0, 0},
			new byte[] {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
			new byte[] {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
			new byte[] {0, 0, 0, 0, 0, 0, 0, 0, 0, 0}
		};
		Glass glass = new Glass(data);
		
		Assert.assertEquals("figure to glass", figure.toGlass(), glass);
	}
	
	@Test
	public void testApplyCommand() {
		Figure figure = Figure.createFigure(FigureData.I, 5, 2);
		Command command1 = new Command(2, Angle.ANGLE_90, true);
		
		byte[][] expectedData1 = new byte[][] {
			new byte[] {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
			new byte[] {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
			new byte[] {0, 0, 0, 0, 0, 1, 1, 1, 1, 0},
			new byte[] {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
			new byte[] {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
			new byte[] {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
			new byte[] {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
			new byte[] {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
			new byte[] {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
			new byte[] {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
			new byte[] {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
			new byte[] {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
			new byte[] {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
			new byte[] {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
			new byte[] {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
			new byte[] {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
			new byte[] {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
			new byte[] {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
			new byte[] {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
			new byte[] {0, 0, 0, 0, 0, 0, 0, 0, 0, 0}
		};
		
		Glass expectedGlass1 = new Glass(expectedData1);
		Glass actualGlass1 = figure.applyCommand(command1).toGlass();
		Assert.assertEquals("positive shift x", actualGlass1, expectedGlass1);
		
		Command command2 = new Command(-2, Angle.ANGLE_180, true);
		
		byte[][] expectedData2 = new byte[][] {
			new byte[] {0, 0, 0, 1, 0, 0, 0, 0, 0, 0},
			new byte[] {0, 0, 0, 1, 0, 0, 0, 0, 0, 0},
			new byte[] {0, 0, 0, 1, 0, 0, 0, 0, 0, 0},
			new byte[] {0, 0, 0, 1, 0, 0, 0, 0, 0, 0},
			new byte[] {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
			new byte[] {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
			new byte[] {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
			new byte[] {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
			new byte[] {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
			new byte[] {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
			new byte[] {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
			new byte[] {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
			new byte[] {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
			new byte[] {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
			new byte[] {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
			new byte[] {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
			new byte[] {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
			new byte[] {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
			new byte[] {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
			new byte[] {0, 0, 0, 0, 0, 0, 0, 0, 0, 0}
		};
		
		Assert.assertEquals("negative shift x", figure.applyCommand(command2).toGlass(), new Glass(expectedData2));
	}
	
	@Test
	public void testDropToGlass() {
		byte[][] data = new byte[][] {
			new byte[] {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
			new byte[] {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
			new byte[] {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
			new byte[] {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
			new byte[] {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
			new byte[] {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
			new byte[] {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
			new byte[] {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
			new byte[] {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
			new byte[] {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
			new byte[] {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
			new byte[] {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
			new byte[] {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
			new byte[] {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
			new byte[] {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
			new byte[] {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
			new byte[] {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
			new byte[] {0, 0, 1, 1, 1, 1, 1, 1, 1, 1},
			new byte[] {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
			new byte[] {0, 0, 0, 0, 0, 0, 0, 0, 0, 0}
		};
		Glass glass = new Glass(data);
		
		Figure figure = Figure.createFigure(FigureData.I, 4, 0);
		
		byte[][] expectedData = new byte[][] {
			new byte[] {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
			new byte[] {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
			new byte[] {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
			new byte[] {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
			new byte[] {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
			new byte[] {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
			new byte[] {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
			new byte[] {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
			new byte[] {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
			new byte[] {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
			new byte[] {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
			new byte[] {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
			new byte[] {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
			new byte[] {0, 0, 0, 0, 1, 0, 0, 0, 0, 0},
			new byte[] {0, 0, 0, 0, 1, 0, 0, 0, 0, 0},
			new byte[] {0, 0, 0, 0, 1, 0, 0, 0, 0, 0},
			new byte[] {0, 0, 0, 0, 1, 0, 0, 0, 0, 0},
			new byte[] {0, 0, 1, 1, 1, 1, 1, 1, 1, 1},
			new byte[] {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
			new byte[] {0, 0, 0, 0, 0, 0, 0, 0, 0, 0}
		};
		Glass expectedGlass = new Glass(expectedData);
		
		Glass actualGlass = figure.dropToGlass(glass);
		Assert.assertEquals("drop to glass", actualGlass, expectedGlass);
	}
}
