package domain.strategy.estimation;

import org.junit.Assert;
import org.junit.Test;

import domain.Glass;

public class FilledRowCriterionTest {
	@Test
	public void test() {
		byte[][] data = new byte[][] {
			new byte[] {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
			new byte[] {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
			new byte[] {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
			new byte[] {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
			new byte[] {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
			new byte[] {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
			new byte[] {0, 0, 0, 0, 0, 1, 0, 0, 0, 0},
			new byte[] {0, 0, 0, 0, 0, 1, 0, 0, 0, 0},
			new byte[] {0, 0, 0, 0, 0, 1, 0, 0, 0, 0},
			new byte[] {0, 0, 0, 0, 0, 1, 0, 0, 0, 0},
			new byte[] {0, 0, 0, 0, 0, 1, 0, 0, 0, 0},
			new byte[] {0, 0, 0, 0, 0, 1, 0, 0, 0, 0},
			new byte[] {0, 0, 0, 0, 0, 1, 0, 0, 0, 0},
			new byte[] {1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
			new byte[] {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
			new byte[] {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
			new byte[] {1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
			new byte[] {1, 1, 1, 1, 1, 1, 1, 1, 1, 0},
			new byte[] {1, 1, 1, 1, 1, 0, 1, 1, 1, 1},
			new byte[] {1, 1, 1, 1, 1, 1, 1, 1, 1, 1}
		};
		
		Glass glass = new Glass(data);
		FilledRowCriterion criterion = new FilledRowCriterion();
		Assert.assertEquals("filled rows", 3, criterion.estimate(glass));
	}
}
