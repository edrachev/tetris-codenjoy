package domain.strategy.estimation;

import org.junit.Assert;
import org.junit.Test;

import domain.Glass;

public class NumberHolesCriterionTest {
	@Test
	public void test() {
		byte[][] data = new byte[][] {
			new byte[] {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
			new byte[] {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
			new byte[] {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
			new byte[] {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
			new byte[] {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
			new byte[] {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
			new byte[] {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
			new byte[] {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
			new byte[] {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
			new byte[] {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
			new byte[] {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
			new byte[] {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
			new byte[] {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
			new byte[] {0, 0, 1, 0, 1, 1, 0, 1, 0, 0},
			new byte[] {1, 0, 0, 0, 0, 0, 0, 0, 0, 1},
			new byte[] {1, 0, 0, 0, 0, 0, 0, 0, 0, 0},
			new byte[] {0, 0, 0, 0, 0, 0, 0, 0, 0, 1},
			new byte[] {1, 1, 0, 1, 1, 1, 1, 1, 1, 0},
			new byte[] {0, 1, 1, 1, 1, 1, 1, 1, 1, 0},
			new byte[] {1, 1, 1, 1, 1, 1, 1, 1, 1, 1}
		};
		
		Glass glass = new Glass(data);
		NumberHolesCriterion criterion = new NumberHolesCriterion();
		int estimation = criterion.estimate(glass);
		Assert.assertEquals("number holes", 11, estimation);
	}
}
