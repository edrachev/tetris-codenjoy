package domain.strategy.estimation;

import org.junit.Assert;
import org.junit.Test;

import domain.Glass;

public class NumberClosedCellsCriterionTest {
	@Test
	public void test() {
		byte[][] data = new byte[][] {
			new byte[] {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
			new byte[] {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
			new byte[] {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
			new byte[] {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
			new byte[] {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
			new byte[] {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
			new byte[] {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
			new byte[] {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
			new byte[] {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
			new byte[] {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
			new byte[] {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
			new byte[] {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
			new byte[] {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
			new byte[] {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
			new byte[] {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
			new byte[] {0, 0, 1, 0, 0, 0, 0, 0, 0, 0},
			new byte[] {1, 1, 0, 1, 1, 1, 1, 0, 1, 1},
			new byte[] {1, 1, 1, 0, 1, 1, 1, 1, 1, 0},
			new byte[] {1, 1, 1, 0, 1, 0, 1, 1, 1, 1},
			new byte[] {1, 1, 1, 0, 1, 1, 1, 0, 1, 1}
		};
		
		Glass glass = new Glass(data);
		
		NumberClosedCellsCriterion criterion = new NumberClosedCellsCriterion();
		int estimation = criterion.estimate(glass);
		Assert.assertEquals("closed cells estimation", 7, estimation);
	}
}
