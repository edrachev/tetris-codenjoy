package domain.strategy;

import org.junit.Assert;
import org.junit.Test;

import domain.Angle;
import domain.Command;
import domain.Glass;
import domain.figure.Figure;
import domain.figure.Figure.FigureData;

public class BustStrategyTest {
	@Test
	public void testSimpleO() {
		byte[][] data = new byte[][] {
			new byte[] {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
			new byte[] {1, 1, 0, 0, 0, 0, 0, 0, 0, 0},
			new byte[] {1, 1, 0, 0, 0, 0, 0, 0, 0, 0},
			new byte[] {1, 1, 0, 0, 0, 0, 0, 0, 0, 0},
			new byte[] {1, 1, 0, 0, 0, 0, 0, 0, 0, 0},
			new byte[] {1, 1, 0, 0, 0, 0, 0, 0, 0, 0},
			new byte[] {1, 1, 0, 0, 0, 0, 0, 0, 0, 0},
			new byte[] {1, 1, 0, 0, 0, 0, 0, 0, 0, 0},
			new byte[] {1, 1, 0, 0, 0, 0, 0, 0, 0, 0},
			new byte[] {1, 1, 0, 0, 0, 0, 0, 0, 0, 0},
			new byte[] {1, 1, 0, 0, 1, 1, 0, 0, 0, 0},
			new byte[] {1, 1, 0, 0, 1, 1, 0, 0, 0, 0},
			new byte[] {1, 1, 0, 0, 1, 1, 0, 0, 0, 0},
			new byte[] {1, 1, 0, 0, 1, 1, 0, 0, 0, 0},
			new byte[] {1, 1, 0, 0, 1, 1, 0, 0, 0, 0},
			new byte[] {1, 1, 0, 0, 1, 1, 0, 0, 0, 0},
			new byte[] {1, 1, 0, 0, 1, 1, 0, 0, 0, 0},
			new byte[] {1, 1, 0, 0, 1, 1, 0, 0, 0, 0},
			new byte[] {1, 1, 0, 0, 1, 1, 0, 0, 0, 0},
			new byte[] {1, 1, 0, 0, 1, 1, 0, 0, 0, 0}
		};
		
		Glass glass = new Glass(data);
		
		Figure figure = Figure.createFigure(FigureData.O, 4, 0);
		Figure nextFigure = Figure.createFigure(FigureData.O, 4, 0);
		BustStrategy strategy = new BustStrategy();
		Command command = strategy.apply(glass, figure, nextFigure);
		Command expectedCommand = new Command(-2, Angle.ANGLE_0, true);
		Assert.assertEquals("simple O", expectedCommand, command);
	}
	
	@Test
	public void testSimpleJ() {
		byte[][] data = new byte[][] {
			new byte[] {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
			new byte[] {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
			new byte[] {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
			new byte[] {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
			new byte[] {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
			new byte[] {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
			new byte[] {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
			new byte[] {0, 1, 1, 1, 1, 1, 1, 1, 1, 1},
			new byte[] {0, 1, 1, 1, 1, 1, 1, 1, 1, 1},
			new byte[] {0, 1, 1, 1, 1, 1, 1, 1, 1, 1},
			new byte[] {0, 1, 1, 1, 1, 1, 1, 1, 1, 1},
			new byte[] {0, 1, 1, 1, 1, 1, 1, 1, 1, 1},
			new byte[] {0, 1, 1, 1, 1, 1, 1, 1, 1, 1},
			new byte[] {0, 1, 1, 1, 1, 1, 1, 1, 1, 1},
			new byte[] {0, 1, 1, 1, 1, 1, 1, 1, 1, 1},
			new byte[] {0, 1, 1, 1, 1, 1, 1, 1, 1, 1},
			new byte[] {1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
			new byte[] {1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
			new byte[] {1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
			new byte[] {1, 1, 1, 1, 1, 1, 1, 1, 1, 1}
		};
		
		Glass glass = new Glass(data);
		
		Figure figure = Figure.createFigure(FigureData.J, 4, 0);
		Figure nextFigure = Figure.createFigure(FigureData.J, 4, 0);
		BustStrategy strategy = new BustStrategy();
		Command command = strategy.apply(glass, figure, nextFigure);
		Command expectedCommand = new Command(-4, Angle.ANGLE_180, true);
		Assert.assertEquals("simple J", expectedCommand, command);
	}
}
