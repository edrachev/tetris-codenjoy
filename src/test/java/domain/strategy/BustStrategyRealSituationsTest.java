package domain.strategy;

import org.junit.Assert;
import org.junit.Test;

import domain.Angle;
import domain.Command;
import domain.Glass;
import domain.figure.Figure;
import domain.figure.Figure.FigureData;

public class BustStrategyRealSituationsTest {
	@Test
	public void testJ() {
		Figure figure = Figure.createFigure(FigureData.J, 4, 1);
		Figure nextFigure = Figure.createFigure(FigureData.J, 4, 1);
		Glass glass = Glass.createFromString(" ** ******  *  *****                                                                                                                                                                                    ");
		BustStrategy strategy = new BustStrategy();
		Command command = strategy.apply(glass, figure, nextFigure);
		Command expectedCommand = new Command(-4, Angle.ANGLE_180, true);
		Assert.assertEquals("testJ", expectedCommand, command);
	}
}
